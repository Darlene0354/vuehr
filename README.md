菜单项数据加载成功之后，在前端有几个可以存放的地方

1.sessonStorage
2.localStorage

比前两个安全，不易被看到
状态管理，数据共享
相当于保存在浏览器上
**3.vuex**
npm install vuex

##权限管理
通过后端相应的角色进行管理，接口设计时需要相应的权限，保证数据安全
前端只是为了用户体验进行访问页面限制
ssm+shiro
springboot微服务+springSecurity
