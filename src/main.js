import Vue from 'vue'
import ElementUI from 'element-ui';
import App from './App.vue'
import router from './router'
import store from './store'

/**
 * 将请求方法挂到vue上,后面需要发送网络请求时，不需要导入api
 */
//1.导入所有的请求方法
import {postRequest} from "@/utils/api";
import {postKeyValueRequest} from "@/utils/api";
import {putRequest} from "@/utils/api";
import {deleteRequest} from "@/utils/api";
import {getRequest} from "@/utils/api";
import {initMenu} from "@/utils/menu";
import 'font-awesome/css/font-awesome.min.css'
//2.将请求方法添加到vue.prototype上
Vue.prototype.postRequest = postRequest;

Vue.prototype.postKeyValueRequest = postKeyValueRequest;
Vue.prototype.putRequest = putRequest;
Vue.prototype.deleteRequest = deleteRequest;
Vue.prototype.getRequest = getRequest;

Vue.config.productionTip=false
Vue.use(ElementUI,{size:'small'});

// 全局导航首页，页面跳转前监听
router.beforeEach((to, from, next) => {
  //1. 如果要去的页面是登录页面，直接过
  // 2. 如果不是登录页面，先从store中获取当前的登录状态。如果未登录，通过路由中的meta属性的requireAuth属性判断要去的页面是否需要登录，如果需要登录，调回登录页面，如果不需要登录，直接过。如果已经登陆了，先初始化菜单，再跳转
  if(to.path === '/'){
    next();
  }else{
    // 从sessionStorage中拿到用户登陆数据，判断用户是否登陆
    if(window.sessionStorage.getItem("user")){
      initMenu(router,store);
      next();
    }else{
      // 没有登录
      next('/?redirect='+to.path);
    }
  }
})

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
