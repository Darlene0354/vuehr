import {getRequest} from "@/utils/api";

// 浏览器刷新后保证菜单栏还在，可以使用路由导航守卫
export const initMenu=(router,store)=>{
    //判断store中的数据是否存在，如果存在，说明这次跳转是正常的跳转，而不是用户按F5或者直接在地址栏输入某个地址进入的。否则去加载菜单。
    if(store.state.routes.length>0){
        return;
    }
    getRequest("/system/config/menu").then(data=>{
        if(data){
            //通过formatRoutes方法将服务器返回的json转为router需要的格式，转component,因为服务端返回的component事一个字符串，router中需要的事一个组件，在firmatRoutes方法中动态的加载需要的组件。
            let fmtRoutes = formatRoutes(data);
            //一方面将数据存入store中，另一方面利用路由中的addRoutes方法将它动态的添加到路由中
            router.addRoutes(fmtRoutes);
            store.commit('initRoutes',fmtRoutes);
        }
    })

}
export const formatRoutes=(routes)=>{
    let fmRoutes=[];
    routes.forEach(router=>{
        let{
            path,
            component,
            name,
            meta,
            iconCls,
            children
        }=router;
        if(children && children instanceof Array){
            children=formatRoutes(children);
        }
        let fmRouter={
            path:path,
            name:name,
            iconCls:iconCls,
            meta:meta,
            children:children,
            component(resolve) {
                // if (component.startsWith("Home")) {
                //     require(['../views/' + component + '.vue'], resolve);
                // } else if (component == "EmpBasic") {
                //     require(['../views/emp/' + component + '.vue'], resolve);
                // } else if (component == "PerEmp") {
                //     require(['../views/per/' + component + '.vue'], resolve);
                // }else if (component == "SalSobCfg") {
                //     require(['../views/sal/' + component + '.vue'], resolve);
                // } else if (component == "SalSob") {
                //     require(['../views/sal/' + component + '.vue'], resolve);
                // } else if (component == "SysHr") {
                //     require(['../views/sys/' + component + '.vue'], resolve);
                // } else if (component == "SysBasic") {
                //     require(['../views/sys/' + component + '.vue'], resolve);
                // }
                if (component.startsWith("Home")) {
                    require(['../views/' + component + '.vue'], resolve);
                } else if (component.startsWith("Emp")) {
                    require(['../views/emp/' + component + '.vue'], resolve);
                } else if (component.startsWith("Per")) {
                    require(['../views/per/' + component + '.vue'], resolve);
                } else if (component.startsWith("Sal")) {
                    require(['../views/sal/' + component + '.vue'], resolve);
                } else if (component.startsWith("Sta")) {
                    require(['../views/sta/' + component + '.vue'], resolve);
                } else if (component.startsWith("Sys")) {
                    require(['../views/sys/' + component + '.vue'], resolve);
                }
            }
        }
        fmRoutes.push(fmRouter);
    })
    return fmRoutes;
}