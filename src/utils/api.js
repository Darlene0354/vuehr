import axios from 'axios'
import {Message} from "element-ui";
import router from '../router'

/**
 * 响应拦截器
 * 采用axios处理网络请求，避免在每次请求时判断各种网络情况（连接超时，服务器内部错误，权限不足等），对axios进行简单的封装，使用axios的拦截器功能
 */
axios.interceptors.response.use(success=>{
    // 展示错误信息
    if(success.status && success.status===200 &&success.data.status===500){
        //业务错误
        Message.error({message:success.data.msg})
        //返回空
        return;
    }
    //成功，自动提示服务端message
    if(success.data.msg){
        Message.success({message:success.data.msg})
    }
    //返回到请求调用的地方
    return success.data;
},error=>{
    if(error.response.status===504||error.response.status===404){
        Message.error({message:'服务器被吃了------'})
    }else if(error.response.status===403){
        Message.error({message:'权限不足，请联系管理员'})
    }else if(error.response.status===401){

        Message.error({message:'尚未登陆，请登陆'})
        router.replace('/')
    }else{
        if(error.response.data.msg){
            // 服务端有错误消息
            Message.error({message:error.response.data.msg})
        }else{
            //服务端没有返回错误信息
            Message.error({message:'未知错误'})
        }

    }
    return;
})

let base='';
//登录请求默认key value形式传递参数，不支持json数据格式传参
export const postKeyValueRequest=(url,params)=>{
    return axios({
        method:'post',
        // 不是单引号，表示变量
        url:`${base}${url}`,
        data:params,
        transformRequest:[function (data){
            let ret='';
            for(let i in data){
                ret+=encodeURIComponent(i)+'='+encodeURIComponent(data[i])+'&'
            }
            return ret;
        }],
        headers:{
            'Content-Type':'application/x-www-form-urlencoded'
        }
    });
}
//封装请求方法，post put get delete,json形式传递数据
// 将其做成插件，不然每次使用都要加载 main.js
export const postRequest=(url,params)=>{
    return axios({
        method:'post',
        url:`${base}${url}`,
        data:params
    })
}
export const putRequest=(url,params)=>{
    return axios({
        method:'put',
        url:`${base}${url}`,
        data:params
    })
}
export const getRequest=(url,params)=>{
    return axios({
        method:'get',
        url:`${base}${url}`,
        data:params
    })
}
export const deleteRequest=(url,params)=>{
    return axios({
        method:'delete',
        url:`${base}${url}`,
        data:params
    })
}