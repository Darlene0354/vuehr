import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
/**
 * 最开始的界面：index.js->main.js->App.vue->router.js
 * 当用户注销登陆时，将localStorage中的数据清除
 */
export default new Vuex.Store({
    state:{
        routes:[]
    },
    mutations:{
        initRoutes(state,data){
            state.routes = data;
        }
    },
    actions:{

    }
})
