import Vue from 'vue'
import Router from 'vue-router'
import 'element-ui/lib/theme-chalk/index.css';
import Login from './views/Login.vue'
import Home from './views/Home.vue'
Vue.use(Router)
/**
 * 引用页面
 */
export default new Router({
    routes:[{
        path: '/',
        name: 'Login',
        component: Login,
        hidden: true
    },{
        path: '/Home',
        name: '测试菜单',
        component: Home,
        hidden: true,

    }

    ]
})